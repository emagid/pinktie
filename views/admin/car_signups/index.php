<?php
	if(count($model->car_signups) > 0){ ?>
		<div class="box box-table">
			<table class="table">
				<thead>
			        <tr>
			            <th width="15%">Name</th>
			            <th width="15%">Email</th>
			          	<th width="5%" class="text-center">Edit</th>
			        	<th width="5%" class="text-center">Delete</th>	
			        </tr>
	      		</thead>
	      		<tbody>
		       	<?php foreach($model->car_signups as $obj){ ?>
			        <tr>
			            <td><a href="<?php echo ADMIN_URL; ?>car_signups/update/<?= $obj->id ?>"><?php echo $obj->name; ?></a></td>
			            <td><a href="<?php echo ADMIN_URL; ?>car_signups/update/<?= $obj->id ?>"><?php echo $obj->email; ?></a></td>
			         	<td class="text-center">
			           		<a class="btn-actions" href="<?= ADMIN_URL ?>car_signups/update/<?= $obj->id ?>">
			           			<i class="icon-pencil"></i> 
			           		</a>
			         	</td>
			         	<td class="text-center">
			           		<a class="btn-actions" href="<?= ADMIN_URL ?>car_signups/delete/<?= $obj->id ?>?token_id=<?php echo get_token();?>" onClick="return confirm('Are You Sure?');">
			             		<i class="icon-cancel-circled"></i> 
			           		</a>
			         	</td>
			       	</tr>
		       	<?php } ?>
		   		</tbody>
			</table>
			<div class="box-footer clearfix">
				<div class='paginationContent'></div>
			</div>
		</div>
	<? } ?>
<?php echo footer(); ?>
<script type="text/javascript">
	var site_url = '<?= ADMIN_URL.'car_signups';?>';
    var total_pages = <?= $model->pagination->total_pages;?>;
    var page = <?= $model->pagination->current_page_index;?>;
</script>