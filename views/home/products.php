<main>
	<section class="product_page" >

        <!-- Header -->
        <header>
            <a href="/"><img src="<?=FRONT_ASSETS?>img/webair.png"></a>
        </header>

        <!-- background -->
        <video class='inner_page_hero' style='z-index: -1;' autoplay loop>
            <source src="<?= FRONT_ASSETS ?>img/home_vid.mp4" type="video/mp4">      
        </video>
        <!-- <div class='inner_page_hero'>
            <div class='overlay'><h1>OUR SERVICES</h1></div>
        </div> -->

        <!-- home button -->
        <a href="/"><aside id='home_click_white' class='home_click'>
            <img class='white_img' src="<?=FRONT_ASSETS?>img/home.png"> 
        </aside></a>


        <!-- CHOICES -->
            <div class='options products'>
                <!-- <a class='option click_action' href="healthcare"><img src="<?=FRONT_ASSETS?>img/healthcare.png"><p>HEALTHCARE</p></a> -->
                <a class='option click_action' href="privatecloud"><img src="<?=FRONT_ASSETS?>img/private_cloud.png"><p>PRIVATE CLOUD</p></a>
                <!-- <a class='option click_action' href="management"><img src="<?=FRONT_ASSETS?>img/management.png"><p>MANAGEMENT</p></a> -->
                <a class='option click_action' href="disaster"><img src="<?=FRONT_ASSETS?>img/disaster.png"><p>DISASTER RECOVERY</p></a>
                <a class='option click_action' href="ransomeware"><img src="<?=FRONT_ASSETS?>img/ransomeware.png"><p>RANSOMWARE RECOVERY</p></a>
                <!-- <a class='option click_action' href="ny1"><img src="<?=FRONT_ASSETS?>img/ny1.png"><p>NY1</p></a> -->
                <a class='option click_action' href="hybrid"><img src="<?=FRONT_ASSETS?>img/hybrid.png"><p>HYBRID CLOUD</p></a>
                <a class='option click_action' href="our_network"><img src="<?=FRONT_ASSETS?>img/about.png"><p>OUR NETWORK</p></a>
            </div>
        </section>
</main>

<script type="text/javascript">
    setTimeout(function(){
        $('.options').css('opacity', '1');
        $('.options').css('margin-top', '150px');
        $('.options a').css('display', 'flex');
    }, 1000);
</script>

 