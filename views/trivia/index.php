<main>
    <? $questions = $model->questions?>
	<section class="trivia_page" >

        <!-- Header -->
        <div class='vid_overlay'></div>     
        <div class='vid' style='z-index: -1;'></div>
        <header>
            <a href="/"><img src="<?=FRONT_ASSETS?>img/webair.png"></a>
        </header>

        <!-- trivia questions -->
          <section class="trivia_content trivia">
            <div class="title_holder">
              <p>1 / <?=count($questions)?></p>
            </div>

            <!-- question 1 -->
            <?php foreach ($questions as $i => $question) {?>
                <div class="question_holder" data-fail_text="<?=$question->failure_text?>">
                    <div class="question">
                        <h2>QUESTION <?=$i+1?></h2>
                        <p><?=$question->text?></p>
                        <? if ($question->image != null && $question->image != '') {?>
                            <img src="<?=UPLOAD_URL . 'questions/' . $question->image ?>" width="100"/>
                        <? } ?>
                    </div>

                    <? $choices = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ' ?>

                    <?foreach ($question->answers as $a => $answer) {?>
                    <div class="answer click_action <?=$question->correct_answer_id == $answer->id ? 'correct_answer' : ''?>">
                        <h2><?=substr($choices,$a,1)?></h2>
                        <p><?=$answer->text?></p>
                        <? if ($answer->featured_image != null && $answer->featured_image != '') {?>
                            <img src="<?=UPLOAD_URL . 'answers/' . $answer->featured_image ?>" width="100"/>
                        <? } ?>
                    </div>
                    <? } ?>
                    <button class="button submit">NEXT</button>
                </div>
            <? } ?>
          </section>
            <section class="trivia_completion trivia">

              <div class="question">
                <h2></h2>
                <p>Thanks for Playing</p>

                <p class='prize_text'>Enter your email below for a chance to win an Ipad Pro!</p>
                <img class='ipad' style='height: 312px;' src="<?=FRONT_ASSETS?>img/ipad.jpeg">
                <form id='email'>
                  <input id="name_input" class='jQKeyboard input' type="text" name="name" placeholder='Name'>
                  <input id="email_input" class='jQKeyboard input' type="text" pattern="[A-z0-9._%+-]+@[A-z0-9.-]+\.[A-z]{2,3}$" name="email" placeholder='Email'>
                  <input id="company_input" class='jQKeyboard input' type="text" name="company" placeholder='Company'>
                  <input class="entry-form" type="submit">
                </form>
              </div>


            </section>
        </section>  

    <div class="missing">
      <h2>Name and Email both are required</h2>
    </div>

    <div class='complete'>
      <h2>You've been entered!</h2>
    </div>

    <div class='incomplete'>
      <h2>You already submitted your entry</h2>
    </div>

</main>



<script>
$(document).ready(function () {
      $('.trivia_content').fadeIn('fast');
      $('.trivia_content').css('padding-top', '0px');


          let amount_right = 0
        $(document).on('click', '.answer', function(){

            let failure_text = $(this).parents('.question_holder').data('fail_text');
            
            // if correct answer
            if($(this).hasClass('correct_answer')){ 
              amount_right ++

              // remove ability to choose another answer
                $(this).parents('.question_holder').children('.answer').each(function(){
                    $(this).removeClass('answer click_action');
                    $(this).addClass('answer_outcome');
                });

                var answers = $(this).parent('.question_holder').children('.answer_outcome');
                for ( i=0; i<answers.length; i++ ) {
                  if ( !$(answers[i]).hasClass('correct_answer') ) {
                    $(answers[i]).css('opacity', '0');
                    $(answers[i]).delay(2000).slideUp();
                  }
                }
              
              $(this).after("<div class='answer_outcome correct'> <h2 class='right_answer'>CORRECT</h2></div>");
              $('.correct').slideDown();
              // if ( failure_text !== "" ) {
              //   $(this).after("<div class='answer_outcome correct_info'></div>");
              //   $('.correct_info').append("<p>" + failure_text + "</p>");
              //   $('.correct_info').delay(2500).slideDown();
              // }

              $(this).parent('.question_holder').children('.submit').delay(2500).fadeIn('fast');
       

            // Wrong answer
            } else { 

                // remove ability to choose another answer
                $(this).parents('.question_holder').children('.answer').each(function(){
                    $(this).removeClass('answer click_action');
                    $(this).addClass('answer_outcome');
                });

                var answers = $(this).parent('.question_holder').children('.answer_outcome');
                for ( i=0; i<answers.length; i++ ) {
                  if ( !$(answers[i]).hasClass('correct_answer') ) {
                    $(answers[i]).css('opacity', '0');
                    $(answers[i]).delay(2000).slideUp();
                  }
                }

                $(this).after("<div class='answer_outcome wrong'> <h2 class='right_answer'>INCORRECT</h2></div>");
                $('.wrong').slideDown();
                    $('.wrong').css('opacity', '0');
                    $('.wrong').delay(2000).slideUp();
                // if ( failure_text !== "" ) {
                //   $(this).parents('.question_holder').children('.correct_answer').after("<div class='answer_outcome correct_info'></div>");
                //   $('.correct_info').append("<p>" + failure_text + "</p>");
                //   $('.correct_info').delay(2500).slideDown();
                // }

                $(this).parent('.question_holder').children('.submit').delay(2500).fadeIn('fast');
            }

        });


          // Next questions
          var timer;
          var question_num = 1
          var total_questions = $('.title_holder p').html().substring($('.title_holder p').html().length - 2);
          $(".submit").on({
               'click': function clickAction() {
                   var self = this;
                   $(self).css('pointer-events', 'none');
                   if ( question_num === parseInt(total_questions)) {
                      $('.trivia_content').css('padding-top', '200px');
                      $('.trivia_content').fadeOut('fast');
                   }else {
                      $(self).parent('.question_holder').css('margin-top', '200px');
                      $(self).parent('.question_holder').fadeOut('slow');
                   }

                   parseInt(total_questions)

                    timer = setTimeout(function () {
                      if ( question_num === parseInt(total_questions)) {
                        $('.trivia_completion').fadeIn('slow');
                        $('.trivia_completion').css('padding-top', '0px');
                        $('.trivia_completion .question h2').html(amount_right + " / " + total_questions);
                      }else {
                        $(self).parent('.question_holder').next().fadeIn('slow');
                        $(self).parent('.question_holder').next().css('margin-top', '0px');

                        // Updating question number
                        $('.title_holder p').html((question_num += 1).toString() + "  / " + total_questions )
                      }
                      $(self).css('pointer-events', 'all');

                    }, 1000);
               }
          });

          // $('.entry-form').on("click", function(){
          //   var email = $('#email_input').val();
          //   console.log("This is " + email);
          //   $.post('/trivia/trivia_entry',{email:email},function(data){
          //     console.log(data);
          //   });
          // })

         // Go home after trivia completion
         $("#email input[type='submit']").on('click', function(e){
            e.preventDefault();
            var email = $('#email_input').val();
            var name = $('#name_input').val();
            if(email != '' && name != ''){
                $.post('/trivia/trivia_entry',$('#email').serialize(),function(data){
                  if(data['status'] == true){
                    $('.complete').fadeIn();
                    $('.complete').css('display', 'flex');
                    setTimeout(function(){
                      $('.white').fadeIn();
                        setTimeout(function(){
                            $('#email').submit();
                            window.location = '/';
                        }, 700);
                    }, 3000);
                  }
                  else{
                    $('.incomplete').fadeIn();
                    $('.incomplete').css('display', 'flex');
                    setTimeout(function(){
                      $('.white').fadeIn();
                        setTimeout(function(){
                            $('#email').submit();
                        }, 700);
                    }, 3000);
                  }
                });
            } else {
                $('.missing').fadeIn();
            }
         });
         $('#email').click(function(){
          $('div.jQKeyboardContainer').css('top','785px');
         });
         
    });

</script>


<script type="text/javascript">
    var keyboard;
            $(function(){
                keyboard = {
                    'layout': [
                        // alphanumeric keyboard type
                        // text displayed on keyboard button, keyboard value, keycode, column span, new row
                        [
                            [
                                ['@', '@', 192, 0, true], ['1', '1', 49, 0, false], ['2', '2', 50, 0, false], ['3', '3', 51, 0, false], ['4', '4', 52, 0, false], ['5', '5', 53, 0, false], ['6', '6', 54, 0, false], 
                                ['7', '7', 55, 0, false], ['8', '8', 56, 0, false], ['9', '9', 57, 0, false], ['0', '0', 48, 0, false], ['-', '-', 189, 0, false], ['=', '=', 187, 0, false],
                                ['q', 'q', 81, 0, true], ['w', 'w', 87, 0, false], ['e', 'e', 69, 0, false], ['r', 'r', 82, 0, false], ['t', 't', 84, 0, false], ['y', 'y', 89, 0, false], ['u', 'u', 85, 0, false], 
                                ['i', 'i', 73, 0, false], ['o', 'o', 79, 0, false], ['p', 'p', 80, 0, false], ['[', '[', 219, 0, false], [']', ']', 221, 0, false], ['&#92;', '\\', 220, 0, false],
                                ['a', 'a', 65, 0, true], ['s', 's', 83, 0, false], ['d', 'd', 68, 0, false], ['f', 'f', 70, 0, false], ['g', 'g', 71, 0, false], ['h', 'h', 72, 0, false], ['j', 'j', 74, 0, false], 
                                ['k', 'k', 75, 0, false], ['l', 'l', 76, 0, false], [';', ';', 186, 0, false], ['&#39;', '\'', 222, 0, false], ['Enter', '13', 13, 3, false],
                                ['Shift', '16', 16, 2, true], ['z', 'z', 90, 0, false], ['x', 'x', 88, 0, false], ['c', 'c', 67, 0, false], ['v', 'v', 86, 0, false], ['b', 'b', 66, 0, false], ['n', 'n', 78, 0, false], 
                                ['m', 'm', 77, 0, false], [',', ',', 188, 0, false], ['.', '.', 190, 0, false], ['/', '/', 191, 0, false], ['Shift', '16', 16, 2, false],
                                ['Bksp', '8', 8, 3, true], ['Space', '32', 32, 12, false], ['Clear', '46', 46, 3, false], ['Cancel', '27', 27, 3, false]
                            ]
                        ]
                    ]
                }
                $('input.jQKeyboard').initKeypad({'keyboardLayout': keyboard});


                var board = {
                    'layout': [
                        // alphanumeric keyboard type
                        // text displayed on keyboard button, keyboard value, keycode, column span, new row
                        [
                                ['@', '@', 192, 0, true], ['1', '1', 49, 0, false], ['2', '2', 50, 0, false], ['3', '3', 51, 0, false], ['4', '4', 52, 0, false], ['5', '5', 53, 0, false], ['6', '6', 54, 0, false], 
                                ['7', '7', 55, 0, false], ['8', '8', 56, 0, false], ['9', '9', 57, 0, false], ['0', '0', 48, 0, false], ['Bksp', '8', 8, 3, true]
                        ]
                    ]
                }
                $('input.donate_key').initKeypad({'donateKeyboardLayout': board});
            });
  </script>

  <link rel="stylesheet" type="text/css" href="<?=FRONT_CSS?>jQKeyboard.css">

  <script>
      (function($){

    var keyboardLayout = {
        'layout': [
            // alphanumeric keyboard type
            // text displayed on keyboard button, keyboard value, keycode, column span, new row
            [
                [
                    ['@', '@', 192, 0, true], ['1', '1', 49, 0, false], ['2', '2', 50, 0, false], ['3', '3', 51, 0, false], ['4', '4', 52, 0, false], ['5', '5', 53, 0, false], ['6', '6', 54, 0, false], 
                    ['7', '7', 55, 0, false], ['8', '8', 56, 0, false], ['9', '9', 57, 0, false], ['0', '0', 48, 0, false], ['-', '-', 189, 0, false], ['=', '=', 187, 0, false],
                    ['q', 'q', 81, 0, true], ['w', 'w', 87, 0, false], ['e', 'e', 69, 0, false], ['r', 'r', 82, 0, false], ['t', 't', 84, 0, false], ['y', 'y', 89, 0, false], ['u', 'u', 85, 0, false], 
                    ['i', 'i', 73, 0, false], ['o', 'o', 79, 0, false], ['p', 'p', 80, 0, false], ['[', '[', 219, 0, false], [']', ']', 221, 0, false], ['&#92;', '\\', 220, 0, false],
                    ['a', 'a', 65, 0, true], ['s', 's', 83, 0, false], ['d', 'd', 68, 0, false], ['f', 'f', 70, 0, false], ['g', 'g', 71, 0, false], ['h', 'h', 72, 0, false], ['j', 'j', 74, 0, false], 
                    ['k', 'k', 75, 0, false], ['l', 'l', 76, 0, false], [';', ';', 186, 0, false], ['&#39;', '\'', 222, 0, false], ['Enter', '13', 13, 3, false],
                    ['Shift', '16', 16, 2, true], ['z', 'z', 90, 0, false], ['x', 'x', 88, 0, false], ['c', 'c', 67, 0, false], ['v', 'v', 86, 0, false], ['b', 'b', 66, 0, false], ['n', 'n', 78, 0, false], 
                    ['m', 'm', 77, 0, false], [',', ',', 188, 0, false], ['.', '.', 190, 0, false], ['/', '/', 191, 0, false], ['Shift', '16', 16, 2, false],
                    ['Bksp', '8', 8, 3, true], ['Space', '32', 32, 12, false], ['Clear', '46', 46, 3, false], ['Cancel', '27', 27, 3, false]
                ],
                [
                    ['~', '~', 192, 0, true], ['!', '!', 49, 0, false], ['@', '@', 50, 0, false], ['#', '#', 51, 0, false], ['$', '$', 52, 0, false], ['%', '%', 53, 0, false], ['^', '^', 54, 0, false], 
                    ['&', '&', 55, 0, false], ['*', '*', 56, 0, false], ['(', '(', 57, 0, false], [')', ')', 48, 0, false], ['_', '_', 189, 0, false], ['+', '+', 187, 0, false],
                    ['Q', 'Q', 81, 0, true], ['W', 'W', 87, 0, false], ['E', 'E', 69, 0, false], ['R', 'R', 82, 0, false], ['T', 'T', 84, 0, false], ['Y', 'Y', 89, 0, false], ['U', 'U', 85, 0, false], 
                    ['I', 'I', 73, 0, false], ['O', 'O', 79, 0, false], ['P', 'P', 80, 0, false], ['{', '{', 219, 0, false], ['}', '}', 221, 0, false], ['|', '|', 220, 0, false],
                    ['A', 'A', 65, 0, true], ['S', 'S', 83, 0, false], ['D', 'D', 68, 0, false], ['F', 'F', 70, 0, false], ['G', 'G', 71, 0, false], ['H', 'H', 72, 0, false], ['J', 'J', 74, 0, false], 
                    ['K', 'K', 75, 0, false], ['L', 'L', 76, 0, false], [':', ':', 186, 0, false], ['"', '"', 222, 0, false], ['Enter', '13', 13, 3, false],
                    ['Shift', '16', 16, 2, true], ['Z', 'Z', 90, 0, false], ['X', 'X', 88, 0, false], ['C', 'C', 67, 0, false], ['V', 'V', 86, 0, false], ['B', 'B', 66, 0, false], ['N', 'N', 78, 0, false], 
                    ['M', 'M', 77, 0, false], ['<', '<', 188, 0, false], ['>', '>', 190, 0, false], ['?', '?', 191, 0, false], ['Shift', '16', 16, 2, false],
                    ['Bksp', '8', 8, 3, true], ['Space', '32', 32, 12, false], ['Clear', '46', 46, 3, false], ['Cancel', '27', 27, 3, false]  
                ]
            ]
        ]
    };


        var donateKeyboardLayout = {
        'layout': [
            // alphanumeric keyboard type
            // text displayed on keyboard button, keyboard value, keycode, column span, new row
            [
                [
                    ['1', '1', 49, 0, true], ['2', '2', 50, 0, false], ['3', '3', 51, 0, false], ['4', '4', 52, 0, false], ['5', '5', 53, 0, false], ['6', '6', 54, 0, false], 
                    ['7', '7', 55, 0, false], ['8', '8', 56, 0, false], ['9', '9', 57, 0, false], ['0', '0', 48, 0, false], ['Bksp', '8', 8, 3, true]
                ]
            ]
        ]
    };

    var activeInput = {
        'htmlElem': '',
        'initValue': '',
        'keyboardLayout': keyboardLayout,
        'donateKeyboardLayout': donateKeyboardLayout,
        'keyboardType': '0',
        'keyboardSet': 0,
        'dataType': 'string',
        'isMoney': false,
        'thousandsSep': ',',
        'disableKeyboardKey': false
    };

    /*
     * initialize keyboard
     * @param {type} settings
     */
    $.fn.initKeypad = function(settings){
        //$.extend(activeInput, settings);

        $(this).click(function(e){
            $("#jQKeyboardContainer").remove();
            activateKeypad(e.target);
        });
    };
    
    /*
     * create keyboard container and keyboard button
     * @param {DOM object} targetInput
     */
    function activateKeypad(targetInput){
        if($('div.jQKeyboardContainer').length === 0)
        {
            activeInput.htmlElem = $(targetInput);
            activeInput.initValue = $(targetInput).val();

            $(activeInput.htmlElem).addClass('focus');
            createKeypadContainer();
            createKeypad(0);
        }
    }
    
    /*
     * create keyboard container
     */
    function createKeypadContainer(){
        var container = document.createElement('div');
        container.setAttribute('class', 'jQKeyboardContainer');
        container.setAttribute('id', 'jQKeyboardContainer');
        container.setAttribute('name', 'keyboardContainer' + activeInput.keyboardType);
        
        $('body').append(container);
    }
    
    /*
     * create keyboard
     * @param {Number} set
     */
    function createKeypad(set){
        $('#jQKeyboardContainer').empty();
        if ( $('input.donateinput').is(':focus') ) {
            var layout = activeInput.donateKeyboardLayout.layout[activeInput.keyboardType][set];
        }else {
            var layout = activeInput.keyboardLayout.layout[activeInput.keyboardType][set];
        }
        

        for(var i = 0; i < layout.length; i++){

            if(layout[i][4]){
                var row = document.createElement('div');
                row.setAttribute('class', 'jQKeyboardRow');
                row.setAttribute('name', 'jQKeyboardRow');
                $('#jQKeyboardContainer').append(row);
            }

            var button = document.createElement('button');
            button.setAttribute('type', 'button');
            button.setAttribute('name', 'key' + layout[i][2]);
            button.setAttribute('id', 'key' + layout[i][2]);
            button.setAttribute('class', 'jQKeyboardBtn' + ' ui-button-colspan-' + layout[i][3]);
            button.setAttribute('data-text', layout[i][0]);
            button.setAttribute('data-value', layout[i][1]);
            button.innerHTML = layout[i][0];
            
            $(button).click(function(e){
               getKeyPressedValue(e.target); 
            });

            $(row).append(button);
        }
    }
    /*
     * remove keyboard from kepad container
     */
    function removeKeypad(){
        $('#jQKeyboardContainer').remove();
        $(activeInput.htmlElem).removeClass('focus');
    }
    
    /*
     * handle key pressed
     * @param {DOM object} clickedBtn
     */
    function getKeyPressedValue(clickedBtn){
        var caretPos = getCaretPosition(activeInput.htmlElem);
        var keyCode = parseInt($(clickedBtn).attr('name').replace('key', ''));
        
        var currentValue = $(activeInput.htmlElem).val();
        var newVal = currentValue;
        var closeKeypad = false;
        
        /*
         * TODO
        if(activeInput.isMoney && activeInput.thousandsSep !== ''){
            stripMoney(currentValue, activeInput.thousandsSep);
        }
        */
        
        switch(keyCode){
            case 8:     // backspace key
                newVal = onDeleteKeyPressed(currentValue, caretPos);
              caretPos--;
                break;
            case 13:    // enter key
                closeKeypad = onEnterKeyPressed();
                break;
            case 16:    // shift key
                onShiftKeyPressed();
                break;
            case 27:    // cancel key
                closeKeypad = true;
                newVal = onCancelKeyPressed(activeInput.initValue);
                break;
            case 32:    // space key
                newVal = onSpaceKeyPressed(currentValue, caretPos);
                caretPos++;
    break;
            case 46:    // clear key
                newVal = onClearKeyPressed();
                break;
            case 190:   // dot key
                newVal = onDotKeyPressed(currentValue, $(clickedBtn), caretPos);
                caretPos++;
                break;
            default:    // alpha or numeric key
                newVal = onAlphaNumericKeyPressed(currentValue, $(clickedBtn), caretPos);
                caretPos++;
                break;
        }
        
        // update new value and set caret position
        $(activeInput.htmlElem).val(newVal);
        setCaretPosition(activeInput.htmlElem, caretPos);

        if(closeKeypad){
            removeKeypad();
            $(activeInput.htmlElem).blur();
        }
    }
    
    /*
     * handle delete key pressed
     * @param value 
     * @param inputType
     */
    function onDeleteKeyPressed(value, caretPos){
        var result = value.split('');
        
        if(result.length > 1){
            result.splice((caretPos - 1), 1);
            return result.join('');
        }
    }
    
    /*
     * handle shift key pressed
     * update keyboard layout and shift key color according to current keyboard set
     */
    function onShiftKeyPressed(){
        var keyboardSet = activeInput.keyboardSet === 0 ? 1 : 0;
        activeInput.keyboardSet = keyboardSet;

        createKeypad(keyboardSet);
        
        if(keyboardSet === 1){
            $('button[name="key16"').addClass('shift-active');
        }else{
            $('button[name="key16"').removeClass('shift-active');
        }
    }
    
    /*
     * handle space key pressed
     * add a space to current value
     * @param {String} curVal
     * @returns {String}
     */
    function onSpaceKeyPressed(currentValue, caretPos){
        return insertValueToString(currentValue, ' ', caretPos);
    }
    
    /*
     * handle cancel key pressed
     * revert to original value and close key pad
     * @param {String} initValue
     * @returns {String}
     */
    function onCancelKeyPressed(initValue){
        return initValue;
    }
    
    /*
     * handle enter key pressed value
     * TODO: need to check min max value
     * @returns {Boolean}
     */
    function onEnterKeyPressed(){
        return true;
    }
    
    /*
     * handle clear key pressed
     * clear text field value
     * @returns {String}
     */
    function onClearKeyPressed(){
        return '';
    }
    
    /*
     * handle dot key pressed
     * @param {String} currentVal
     * @param {DOM object} keyObj
     * @returns {String}
     */
    function onDotKeyPressed(currentValue, keyElement, caretPos){
        return insertValueToString(currentValue, keyElement.attr('data-value'), caretPos);
    }
    
    /*
     * handle all alpha numeric keys pressed
     * @param {String} currentVal
     * @param {DOM object} keyObj
     * @returns {String}
     */
    function onAlphaNumericKeyPressed(currentValue, keyElement, caretPos){
        return insertValueToString(currentValue, keyElement.attr('data-value'), caretPos);
    }
    
    /*
     * insert new value to a string at specified position
     * @param {String} currentValue
     * @param {String} newValue
     * @param {Number} pos
     * @returns {String}
     */
    function insertValueToString(currentValue, newValue, pos){
        var result = currentValue.split('');
        result.splice(pos, 0, newValue);
        
        return result.join('');
    }
    
   /*
    * get caret position
    * @param {DOM object} elem
    * @return {Number}
    */
    function getCaretPosition(elem){
        var input = $(elem).get(0);

        if('selectionStart' in input){    // Standard-compliant browsers
            return input.selectionStart;
        } else if(document.selection){    // IE
            input.focus();
            
            var sel = document.selection.createRange();
            var selLen = document.selection.createRange().text.length;
            
            sel.moveStart('character', -input.value.length);
            return sel.text.length - selLen;
        }
    }
    
    /*
     * set caret position
     * @param {DOM object} elem
     * @param {Number} pos
     */
    function setCaretPosition(elem, pos){
        var input = $(elem).get(0);
        
        if(input !== null) {
            if(input.createTextRange){
                var range = elem.createTextRange();
                range.move('character', pos);
                range.select();
            }else{
                input.focus();
                input.setSelectionRange(pos, pos);
            }
        }
    }
})(jQuery);




</script> 


 