<?php

namespace Model;

class Car_Signup extends \Emagid\Core\Model {
    static $tablename = "public.car_signup";

    public static $fields  =  [
        'name',
        'email',
    ];
}