<?php

class homeController extends siteController {
        function __construct(){
        parent::__construct();
    }    
    public function index(Array $params = []){


        $this->viewData->banners = \Model\Banner::getList(['where'=>"active='1'"]);
        $this->viewData->mainBanner = \Model\Banner::getList(['where'=>"active = 1 and featured_id = 0", 'orderBy'=>"banner_order asc"]);
        $this->viewData->jewelry = \Model\Jewelry::getList(['where' => "active = 1 and featured = 1 and discount != 0", 'orderBy' => "RANDOM()", 'limit' => 2]);

        $this->viewData->diamond = \Model\Product::getItem(null, ['where' => "active = 1 and featured = 1 and discount != 0 and quantity > 0", 'orderBy' => "RANDOM()", 'limit' => 1]);
        $this->viewData->ring = \Model\Ring::getItem(null, ['where' => "active = 1 and featured = 1 and discount != 0", 'orderBy' => "RANDOM()", 'limit' => 1]);
        
        $this->loadView($this->viewData);
    }

    function index_post(){
        $response = ['status'=>false, 'msg'=>'failed'];
        header('Content-Type: application/json');

        if(isset($_POST['email']) && isset($_POST['name']) && $_POST['email'] != '' && $_POST['name'] != ''){
            $email = $_POST['email'];            
            if(\Model\Car_Signup::getItem(null,['where'=>"email = '$email'"]) == ''){
                $new_signup = new \Model\Car_Signup($_POST);
                $new_signup->save();
                if($new_signup->save()){
                    $response['status'] = true;
                    $response['msg'] = "success";
                }
            }
            else{
                $response['status'] = false;
                $response['msg'] ="failed";
            }
            echo json_encode($response);
        }
    }

    function trivia_entry_post(){
        $response = ['status'=>false, 'msg'=>'failed'];
        header('Content-Type: application/json');

        if(isset($_POST['email']) && isset($_POST['name']) && $_POST['email'] != '' && $_POST['name'] != ''){
            $email = $_POST['email'];            
            if(\Model\Trivia_Entry::getItem(null,['where'=>"email = '$email'"]) == ''){
                $new_entry = new \Model\Trivia_Entry($_POST);
                $new_entry->save();
                if($new_entry->save()){
                    $response['status'] = true;
                    $response['msg'] = "success";
                }
            }
            else{
                $response['status'] = false;
                $response['msg'] ="failed";
            }
            echo json_encode($response);
            // if(\Model\Trivia_Entry::getItem(null,['where'=>"email = $email"]) != '')
        }
        
    }



    function photobooth(Array $params = []){
        $this->loadView($this->viewData);
    }

    function about(Array $params = []){
        $this->loadView($this->viewData);
    }

    function answer(Array $params = []){
        $this->loadView($this->viewData);
    }


    function products(Array $params = []){
        $this->loadView($this->viewData);
    }

    function security(Array $params = []){
        $this->loadView($this->viewData);
    }


    function trivia(Array $params = []){
        $this->loadView($this->viewData);
    }

    function healthcare(Array $params = []){
        $this->loadView($this->viewData);
    }

    function privatecloud(Array $params = []){
        $this->loadView($this->viewData);
    }

    function hybrid(Array $params = []){
        $this->loadView($this->viewData);
    }

    function network(Array $params = []){
        $this->loadView($this->viewData);
    }

    function management(Array $params = []){
        $this->loadView($this->viewData);
    }

    function disaster(Array $params = []){
        $this->loadView($this->viewData);
    }

    function ransomeware(Array $params = []){
        $this->loadView($this->viewData);
    }

    function ny1(Array $params = []){
        $this->loadView($this->viewData);
    }

    function car_signup_post(){

    }
}