<?php

class pagesController extends siteController {

	public function index(Array $params = []) {
		redirect(SITE_URL);
	}

	public function page(Array $params = []) {
		$pages = \Model\Page::getItem(null,['where'=>"slug = '{$params['page_slug']}'"]);
		$this->configs['Meta Title'] = $pages->meta_title;
		$this->configs['Meta Keywords'] = $pages->meta_keywords;
		$this->configs['Meta Description'] = $pages->meta_description;
		$this->viewData->page = $pages;
		$this->loadView($this->viewData);
	}

}