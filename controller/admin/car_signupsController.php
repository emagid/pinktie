<?php

class car_signupsController extends adminController {
	
	function __construct(){
		parent::__construct("Car_Signup", "car_signups");
	}
	
	function index(Array $params = []){
		$this->_viewData->hasCreateBtn = true;		

		parent::index($params);
	}

	function update(Array $arr = []){
				
		parent::update($arr);
	}

	function update_post(){
				
		parent::update_post();
	}
  
}