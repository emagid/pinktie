<?php
/**
 * Created by PhpStorm.
 * User: Foran
 * Date: 9/25/2017
 * Time: 9:23 AM
 */

class triviaController extends siteController {
    function __construct()
    {
        parent::__construct();
    }

    function index(Array $params = [])
    {
        $questions = \Model\Question::getList(['orderBy'=>'display_order ASC']);
        foreach ($questions as $i=> $question){
            $questions[$i]->answers = $question->get_answers();
        }
        $this->viewData->questions = $questions;

        $this->loadView($this->viewData);
    }

    function trivia_entry_post(){
        $response = ['status'=>false, 'msg'=>'failed'];
        header('Content-Type: application/json');

        if(isset($_POST['email']) && isset($_POST['name']) && $_POST['email'] != '' && $_POST['name'] != ''){
            $email = $_POST['email'];            
            if(\Model\Trivia_Entry::getItem(null,['where'=>"email = '$email'"]) == ''){
                $new_entry = new \Model\Trivia_Entry($_POST);
                $new_entry->save();
                if($new_entry->save()){
                    $response['status'] = true;
                    $response['msg'] = "success";
                }
            }
            else{
                $response['status'] = false;
                $response['msg'] ="failed";
            }
            echo json_encode($response);
            // if(\Model\Trivia_Entry::getItem(null,['where'=>"email = $email"]) != '')
        }
        
    }
}